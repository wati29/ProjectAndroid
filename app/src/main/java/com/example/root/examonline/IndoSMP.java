package com.example.root.examonline;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class IndoSMP extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_indo_smp);
    }

    public void backIndoSMP(View view) {
        Intent Intent = new Intent(IndoSMP.this, MakulSMP.class);
        startActivity(Intent);
    }

    public void FinishIndoSMP(View view) {
        Intent Intent = new Intent(IndoSMP.this, FInishIndoSD.class);
        startActivity(Intent);
    }
}
