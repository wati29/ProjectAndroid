package com.example.root.examonline;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class PilihTingkat extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pilih_tingkat);
    }

    public void sd(View view) {
        Intent Intent = new Intent(PilihTingkat.this, MakulSD.class);
        startActivity(Intent);
    }

    public void smp(View view) {
        Intent Intent = new Intent(PilihTingkat.this, MakulSMP.class);
        startActivity(Intent);
    }

    public void sma(View view) {
        Intent Intent = new Intent(PilihTingkat.this, MakulSMA.class);
        startActivity(Intent);
    }
}
