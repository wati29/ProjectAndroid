package com.example.root.examonline;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class InggrisSMA extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_inggris_sm);
    }

    public void backIngSMA(View view) {
        Intent Intent = new Intent(InggrisSMA.this, MakulSMA.class);
        startActivity(Intent);
    }

    public void FinishIngSMA(View view) {
        Intent Intent = new Intent(InggrisSMA.this, FInishIndoSD.class);
        startActivity(Intent);
    }
}
