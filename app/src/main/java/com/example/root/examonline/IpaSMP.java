package com.example.root.examonline;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class IpaSMP extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ipa_smp);
    }

    public void backIPASMP(View view) {
        Intent Intent = new Intent(IpaSMP.this, MakulSMP.class);
        startActivity(Intent);
    }

    public void FinishIPASMP(View view) {
        Intent Intent = new Intent(IpaSMP.this, FInishIndoSD.class);
        startActivity(Intent);
    }
}
