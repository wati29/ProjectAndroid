package com.example.root.examonline;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class IndoSMA extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_indo_sm);
    }

    public void backIndoSMA(View view) {
        Intent Intent = new Intent(IndoSMA.this, MakulSMA.class);
        startActivity(Intent);
    }

    public void FinishIndoSMA(View view) {
        Intent Intent = new Intent(IndoSMA.this, FInishIndoSD.class);
        startActivity(Intent);
    }
}
