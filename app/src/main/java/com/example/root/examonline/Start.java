package com.example.root.examonline;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class Start extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_start);
    }

    public void menuMasuk(View view) {
        Intent Intent = new Intent(Start.this, PilihTingkat.class);
        startActivity(Intent);
    }

    public void menuDaftar(View view) {
        Intent Intent = new Intent(Start.this, SignUp.class);
        startActivity(Intent);
    }
}
