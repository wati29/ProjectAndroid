package com.example.root.examonline;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class MtkSD extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mtk_sd);
    }

    public void backMtk(View view) {
        Intent Intent = new Intent(MtkSD.this, MakulSD.class);
        startActivity(Intent);
    }

    public void FinishMtkSD(View view) {
        Intent Intent = new Intent(MtkSD.this, FInishIndoSD.class);
        startActivity(Intent);
    }
}
