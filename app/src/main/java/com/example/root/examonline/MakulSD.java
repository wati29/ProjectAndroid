package com.example.root.examonline;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class MakulSD extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_makul_sd);
    }

    public void IndooSD(View view) {
        Intent Intent = new Intent(MakulSD.this, IndoSD.class);
        startActivity(Intent);
    }

    public void mtkSD(View view) {
        Intent Intent = new Intent(MakulSD.this, MtkSD.class);
        startActivity(Intent);
    }

    public void IpaSD(View view) {
        Intent Intent = new Intent(MakulSD.this, IpaSD.class);
        startActivity(Intent);
    }
}
