package com.example.root.examonline;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class MakulSMP extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_makul_smp);
    }

    public void IngSMP(View view) {
        Intent Intent = new Intent(MakulSMP.this, InggrisSMP.class);
        startActivity(Intent);
    }

    public void MtkSMP(View view) {
        Intent Intent = new Intent(MakulSMP.this, MtkSMP.class);
        startActivity(Intent);
    }

    public void IpaSMP(View view) {
        Intent Intent = new Intent(MakulSMP.this, IpaSMP.class);
        startActivity(Intent);
    }

    public void IndoSMP(View view) {
        Intent Intent = new Intent(MakulSMP.this, IndoSMP.class);
        startActivity(Intent);
    }
}
