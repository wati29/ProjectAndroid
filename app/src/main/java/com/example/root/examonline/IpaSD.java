package com.example.root.examonline;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class IpaSD extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ipa_sd);
    }

    public void backIPA(View view) {
        Intent Intent = new Intent(IpaSD.this, MakulSD.class);
        startActivity(Intent);
    }

    public void FinishIPASD(View view) {
        Intent Intent = new Intent(IpaSD.this, FInishIndoSD.class);
        startActivity(Intent);
    }
}
