package com.example.root.examonline;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class MtkSMP extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mtk_smp);
    }

    public void backMtkSMP(View view) {
        Intent Intent = new Intent(MtkSMP.this, MakulSMP.class);
        startActivity(Intent);
    }

    public void FinishMtkSMP(View view) {
        Intent Intent = new Intent(MtkSMP.this, FInishIndoSD.class);
        startActivity(Intent);
    }
}
