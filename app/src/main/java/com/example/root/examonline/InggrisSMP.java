package com.example.root.examonline;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class InggrisSMP extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_inggris_smp);
    }

    public void backInggrisSMP(View view) {
        Intent Intent = new Intent(InggrisSMP.this, MakulSMP.class);
        startActivity(Intent);
    }

    public void FinishInggrisSMP(View view) {
        Intent Intent = new Intent(InggrisSMP.this, FInishIndoSD.class);
        startActivity(Intent);
    }
}
