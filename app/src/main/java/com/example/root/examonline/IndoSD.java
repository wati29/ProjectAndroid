package com.example.root.examonline;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class IndoSD extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_indo_sd);
    }

    public void result(View view) {
        Intent Intent = new Intent(IndoSD.this, FInishIndoSD.class);
        startActivity(Intent);
    }

    public void backIndo(View view) {
        Intent Intent = new Intent(IndoSD.this, MakulSD.class);
        startActivity(Intent);
    }
}
