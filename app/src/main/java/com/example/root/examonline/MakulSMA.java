package com.example.root.examonline;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class MakulSMA extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_makul_sm);
    }

    public void IndoSMA(View view) {
        Intent Intent = new Intent(MakulSMA.this, IndoSMA.class);
        startActivity(Intent);
    }

    public void MtkSMA(View view) {
        Intent Intent = new Intent(MakulSMA.this, MtkSMA.class);
        startActivity(Intent);
    }

    public void IngSMA(View view) {
        Intent Intent = new Intent(MakulSMA.this, InggrisSMA.class);
        startActivity(Intent);
    }
}
