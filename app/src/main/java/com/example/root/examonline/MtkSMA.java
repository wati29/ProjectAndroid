package com.example.root.examonline;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class MtkSMA extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mtk_sm);
    }

    public void backMtkSMA(View view) {
        Intent Intent = new Intent(MtkSMA.this, MakulSMA.class);
        startActivity(Intent);
    }

    public void FinishMtkSMA(View view) {
        Intent Intent = new Intent(MtkSMA.this, FInishIndoSD.class);
        startActivity(Intent);
    }
}
